# postgres Backup Helm Chart
### Description
This tool helps to make backups from postgres instances, encrypting and uploading them to s3 buckets.
### Stack used
* [gnupg](https://www.gnupg.org/)
* [psql](https://www.postgresql.org/docs/current/)
* [aws cli](https://aws.amazon.com/cli/)
### Parameters
|Name|Description|Value|
|:----|:----|:----:|
|`nameOverride`|String to partially override common.names.fullname template (will maintain the release name)|`""`|
|`fullnameOverride`|String to fully override common.names.fullname template|`""`|
|`namespace`|Resources' namespace|`""`|
|`image.repository`|Name of container image|`""`|
|`image.pullPolicy`|Container image's pull policy|`Always`|
|`image.tag`|Container image's tag|`latest`|
|`registryCredentials.registry`|Registry name|`""`|
|`registryCredentials.username`|Registry username|`""`|
|`registryCredentials.password`|Regsitry password|`""`|
|`imagePullSecrets`|Specify registry secret names as an array|`[]`|
|`daily.enabled`|For running daily cronjob backup|`true`|
|`daily.schedule`|Daily cronjob value|`true`|
|`weekly.enabled`|For running weekly cronjob backup|`true`|
|`weekly.schedule`|Weekly cronjob value|`true`|
|`monthly.enabled`|For running monthly cronjob backup|`true`|
|`monthly.schedule`|Monthly cronjob value|`true`|
|`podAnnotations`|Pod annotation for app name|`"{}"`|
|`serviceAccount.create`|To create a service account|`true`|
|`serviceAccount.annotations`|ServiceAccount Annotations|`"{}"`|
|`serviceAccount.name`|Service account's name|`""`|
|`deployment.vars.POSTGRES_USER`|postgres's username|`""`|
|`deployment.vars.POSTGRES_PASSWORD`|postgres's password|`""`|
|`deployment.vars.POSTGRES_HOST`|postgres's host|`""`|
|`deployment.vars.POSTGRES_PORT`|(optional) postgres's port, default port: 5432|`5432`|
|`deployment.vars.GPG_KEY`|Encryption public key|`""`|
|`deployment.vars.GPG_RECIPIENT`|Encryption recipient|`""`|
|`deployment.vars.S3_PATH`|Bucket path in format "s3://bucket_name/dir/"|`""`|
|`deployment.vars.S3_PROVIDER`|S3 cloud provider,can be aws or generic, default: aws|`generic`|
|`deployment.vars.S3_ENDPOINT`|S3 provider endpoint, required if S3_PROVIDER set to generic value without `https://`|`""`|
|`deployment.vars.AWS_ACCESS_KEY_ID`|S3 provider access key id|`""`|
|`deployment.vars.AWS_SECRET_ACCESS_KEY`|S3 provider secret access key|`""`|
|`deployment.vars.AWS_DEFAULT_REGION`|S3 provider default region|`""`|

### Restore from backup
In order to restore from backup first download from s3 bucket, decrypt the backup and upload it with the following commands:
```
$ gpg --decrypt backup.psql.gz.gpg --output backup.psql.gz 
$ gzip -d -f backup.psql.gz
$ pg_restore backup.psql --dbname=postgresql://$POSTGRES_USER:$POSTGRES_PASSWORD@$POSTGRES_HOST:$POSTGRES_PORT/Database_Name
```
### Note about the env var GPG_KEY
One way of uploading the multiline string GPG_KEY value to helm with the --set flag as the following:
```sh
$ export MULTILINE_VALUE=$(cat <<EOF                     
-----BEGIN PGP PUBLIC KEY BLOCK-----


-----END PGP PUBLIC KEY BLOCK-----
EOF
)
$ helm install -n mysql-backup backup --set deployment.vars.GPG_KEY="$MULTILINE_VALUE" .
```
Please note that this only works if **secrets.yaml** input is set to **stringData**!
